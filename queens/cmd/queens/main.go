// Print total solutions and one of solution with given chessboard size
package main

import (
	"fmt"
	"math/rand"
	"os"
	"strconv"
	"strings"
	"time"

	"gitlab.com/nouse/go-examples/queens"
)

func main() {
	total := 8
	if len(os.Args) == 2 {
		total1, err := strconv.Atoi(os.Args[1])
		if err == nil && total1 > 0 {
			total = total1
		}
	}
	fmt.Printf("Width: %d\n", total)
	queensList := queens.List(total)
	fmt.Printf("Total solutions: %d\n", len(queensList))
	if len(queensList) == 0 {
		os.Exit(0)
	}
	fmt.Println("One solution:")

	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	queens := queensList[r.Intn(len(queensList))]
	var buf strings.Builder
	s := strings.Repeat("o", total-1)
	for _, q := range queens {
		buf.WriteString(s[0:q])
		buf.WriteString("X")
		buf.WriteString(s[q:])
		buf.WriteString("\n")
	}
	fmt.Print(buf.String())
}
