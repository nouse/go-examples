// Package queens will calculate solutions of queens problem
package queens

// Queens is list of position of queens, for example [0 4 7 5 2 6 1 3]
//   means queen of first row is position 0, second row is position 4, and so on.
type Queens []int

type Status struct {
	Queens       Queens
	Total        int
	CurrentLayer int // current layer
	Resolutions  []Queens
	NextLayer    []bool // We save next layer to only allocate once
}

func NewStatus(total int) *Status {
	return &Status{
		Queens:       make(Queens, total),
		Total:        total,
		CurrentLayer: 0,
		Resolutions:  []Queens{},
		NextLayer:    make([]bool, total),
	}
}

func (s *Status) Choices() []int {
	for i := 0; i < s.Total; i++ {
		s.NextLayer[i] = true
	}
	for i, c := range s.Queens[0:s.CurrentLayer] {
		s.NextLayer[c] = false
		a := c + s.CurrentLayer - i
		if a >= 0 && a < s.Total {
			s.NextLayer[a] = false
		}
		b := c - s.CurrentLayer + i
		if b >= 0 && b < s.Total {
			s.NextLayer[b] = false
		}
	}
	final := []int{}
	for i, r := range s.NextLayer {
		if r {
			final = append(final, i)
		}
	}
	return final
}

func (s *Status) FindSolutions(values []int) {
	s.CurrentLayer++
	defer func() { s.CurrentLayer-- }()
	// We just arrived last layer
	if s.CurrentLayer == s.Total {
		result := make(Queens, s.Total)
		copy(result, s.Queens)
		for _, v := range values {
			result[s.Total-1] = v
			s.Resolutions = append(s.Resolutions, result)
		}
		return
	}
	var choices []int
	for _, v := range values {
		s.Queens[s.CurrentLayer-1] = v
		choices = s.Choices()
		if len(choices) > 0 {
			s.FindSolutions(choices)
		}
	}
}

// List return all queens, in BFS way
func List(total int) []Queens {
	queensList := make([]Queens, total)

	for i := 0; i < total; i++ {
		queensList[i] = Queens{i}
	}

	for i := 1; i < total; i++ {
		queensList = Iter(queensList, i, total)
	}
	return queensList
}

// Choices return choices based on current Queens positions
func (queens Queens) Choices(number int) []int {
	currentLayer := len(queens)
	result := make([]int, number)
	for i := 0; i < number; i++ {
		result[i] = i
	}
	for i, c := range queens {
		result[c] = -1
		a := c + currentLayer - i
		if a >= 0 && a < number {
			result[a] = -1
		}
		b := c - currentLayer + i
		if b >= 0 && b < number {
			result[b] = -1
		}
	}
	final := []int{}
	for _, r := range result {
		if r != -1 {
			final = append(final, r)
		}
	}
	return final
}

// Iter to next layer
func Iter(queenList []Queens, layer int, total int) []Queens {
	final := []Queens{}
	for _, queens := range queenList {
		for _, c := range queens.Choices(total) {
			q := make(Queens, layer)
			copy(q, queens)
			final = append(final, append(q, c))
		}
	}
	return final
}

// ListDFS return all queens, in BFS way
func ListDFS(total int) []Queens {
	status := NewStatus(total)

	values := make([]int, total)
	for i := 0; i < total; i++ {
		values[i] = i
	}
	status.FindSolutions(values)
	return status.Resolutions
}
