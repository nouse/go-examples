package queens

import "testing"

func TestList(t *testing.T) {
	list := List(8)
	if len(list) != 92 {
		t.Errorf("Total solutions of Queens 8 should be 92, actual: %d", len(list))
	}
}

func TestListDFS(t *testing.T) {
	list := ListDFS(8)
	if len(list) != 92 {
		t.Errorf("Total solutions of Queens 8 should be 92, actual: %d", len(list))
	}
}

func BenchmarkList(b *testing.B) {
	for i := 0; i < b.N; i++ {
		List(8)
	}
}

func BenchmarkListDFS(b *testing.B) {
	for i := 0; i < b.N; i++ {
		ListDFS(8)
	}
}
