package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"path"
	"path/filepath"
	"sync"
)

var name = flag.String("name", "", "pattern of name")

func main() {
	flag.Parse()

	path := flag.Arg(0)
	var err error
	if path == "" {
		path = "."
	}
	stat, err := os.Lstat(path)
	if err != nil {
		log.Fatal(err)
	}
	if !stat.IsDir() {
		log.Fatalf("%s is not a directory", path)
	}

	output := make(chan string)
	var wg sync.WaitGroup
	wg.Add(1)
	// A channel to control max goroutines, to avoid "too many open files" error
	// Reference: https://stackoverflow.com/a/38825523 https://github.com/golang/go/issues/21621
	sem := make(chan struct{}, 12)
	go search(*name, path, output, &wg, sem)

	writer := bufio.NewWriter(os.Stdout)
	defer writer.Flush()
	go func(ch chan string) {
		for s := range ch {
			writer.WriteString(s)
			writer.WriteString("\n")
		}
	}(output)
	wg.Wait()
}

func search(pattern, directory string, output chan<- string, wg *sync.WaitGroup, sem chan struct{}) {
	sem <- struct{}{}
	defer func() { <-sem }()
	defer wg.Done()
	f, err := os.Open(directory)
	if err != nil {
		fmt.Fprintf(os.Stderr, "error %s\n", err)
	}
	infos, err := f.Readdir(-1)
	f.Close()
	if err != nil {
		fmt.Fprintf(os.Stderr, "error %s\n", err)
	}
	for _, info := range infos {
		basename := path.Base(info.Name())
		realpath := path.Join(directory, info.Name())
		if pattern == "" {
			output <- realpath
		} else if m, _ := filepath.Match(pattern, basename); m {
			output <- realpath
		}
		if info.IsDir() {
			wg.Add(1)
			go search(pattern, realpath, output, wg, sem)
		}
	}
}
