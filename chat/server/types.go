package server

import (
	"errors"
	"sync"
)

type Server struct {
	clients  map[string]client
	entering chan client
	leaving  chan string
	messages chan string
	sync.RWMutex
}

type client struct {
	ch   chan<- string // an outgoing message channel
	name string
}

type dm struct {
	from    string
	to      string
	message string
}

var ErrorInvalidUsername = errors.New("Invalid username")
var ErrorUsernameIsTaken = errors.New("Username is taken")
