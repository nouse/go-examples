package server

import (
	"bufio"
	"fmt"
	"io"
	"regexp"
)

// NewServer will create a new *Server
func NewServer() *Server {
	return &Server{
		clients:  map[string]client{},
		entering: make(chan client),
		leaving:  make(chan string),
		messages: make(chan string),
	}
}

func clientWriter(conn io.Writer, ch <-chan string) {
	for msg := range ch {
		conn.Write([]byte(msg))
		conn.Write([]byte("\n"))
	}
}

func (s *Server) HandleConn(conn io.ReadWriteCloser) {
	ch := make(chan string)

	who, err := s.askName("What is your username?", conn)
	for err != nil {
		if err == io.EOF {
			conn.Close()
			return
		}
		who, err = s.askName(fmt.Sprintf("%s, please try again.", err.Error()), conn)
	}
	go clientWriter(conn, ch)
	ch <- "You are " + who
	s.messages <- who + " has arrived"
	s.entering <- client{name: who, ch: ch}

	input := bufio.NewScanner(conn)
	for input.Scan() {
		s.messages <- who + ": " + input.Text()
	}
	s.leaving <- who
	s.messages <- who + " has left"
	conn.Close()
}

var validUsername = regexp.MustCompile("^[A-Za-z]+[A-Za-z0-9]*$")

func (s *Server) askName(message string, conn io.ReadWriter) (string, error) {
	conn.Write([]byte(message + "\n"))
	input := bufio.NewScanner(conn)
	if !input.Scan() {
		return "", io.EOF
	}

	name := input.Text()
	// validate name
	if !validUsername.MatchString(name) {
		return name, ErrorInvalidUsername
	}
	if s.usernameIsTaken(name) {
		return name, ErrorUsernameIsTaken
	}
	// if name is taken
	return name, nil
}

func (s *Server) usernameIsTaken(username string) bool {
	s.RLock()
	_, exists := s.clients[username]
	s.RUnlock()
	return exists
}

func (s *Server) Broadcaster() {
	for {
		select {
		case msg := <-s.messages:
			for _, cli := range s.clients {
				cli.ch <- msg
			}
		case cli := <-s.entering:
			s.Lock()
			s.clients[cli.name] = cli
			s.Unlock()
		case name := <-s.leaving:
			s.Lock()
			cli := s.clients[name]
			delete(s.clients, name)
			s.Unlock()
			close(cli.ch)
		}
	}
}
