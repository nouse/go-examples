package main

import (
	"log"
	"net"

	"gitlab.com/nouse/go-examples/chat/server"
)

func main() {
	s := server.NewServer()
	listener, err := net.Listen("tcp", "127.0.0.1:8000")
	if err != nil {
		log.Fatal(err)
	}
	go s.Broadcaster()
	log.Println("Chat server is listening on 127.0.0.1:8000")
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Print(err)
			continue
		}
		go s.HandleConn(conn)
	}
}
