package palindrome

import (
	"bufio"
	"fmt"
	"os"
	"testing"
)

func TestLongestPalindromBasic(t *testing.T) {
	testCases := [][2]string{
		{"babad", "aba"},
		{"cbbd", "bb"},
		{"abb", "bb"},
		{"lqlvciwekzxapmjxyddlaoqhfhwphamsyfwjinkfvciucjhdgwodvmnpkibumexvlsxxumvrznuuyqfavmgwfnsvfbrvqhlvhpxaqehsiwxzlvvtxsnmlilbnmvnxyxitxwoahjricdjdncvartepfpdfndxqoozkfpdmlpvshzzffsspdjzlhmamqooooor", "ooooo"},
		{"xxbacaby", "bacab"},
		{"xxbacdcaby", "bacdcab"},
		{"abcdasdfghjkldcba", ""},
		{"aaaabaaa", "aaabaaa"},
		{"bb", "bb"},
	}
	for index, testCase := range testCases {
		t.Run(fmt.Sprintf("Longest palindrome no %d", index), func(it *testing.T) {
			result := longestPalindrome(testCase[0])
			if result != testCase[1] {
				it.Errorf("Expected result of %s is %s, actual %s", testCase[0], testCase[1], result)
			}
		})
	}
}

func TestLongestPalindromFromTestData(t *testing.T) {
	f, err := os.Open("testdata")
	defer f.Close()
	line := 0
	if err != nil {
		t.Fatal("")
	}
	var input, output, result string
	buf := bufio.NewScanner(f)
	for buf.Scan() {
		input = buf.Text()
		line++
		if !buf.Scan() {
			t.Fatalf("Fail to read output of line %d", line)
		}
		output = buf.Text()
		line++
		t.Run(fmt.Sprintf("Longest palindrome line %d-%d", line-1, line), func(it *testing.T) {
			result = longestPalindrome(input)
			if result != output {
				it.Errorf("Fail to verify testdata of line %d-%d, output is %s", line-1, line, result)
			}
		})
	}
}
