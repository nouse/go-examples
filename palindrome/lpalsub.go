package palindrome

import (
	"index/suffixarray"
)

func longestPalindrome(s string) string {
	b := []byte(s)
	r := suffixarray.New(reverse(b))

	if len(r.Lookup(b, 1)) > 0 {
		return s
	}
	var (
		// total length of string
		totalLength = len(b)
		// final result
		result = ""
		// save maxLength globally
		maxLength = 2
	)

	for i := 0; i < totalLength+1-maxLength; i++ {
		toSearch := b[i : i+maxLength]

		indices := r.Lookup(toSearch, -1)
		if len(indices) == 0 {
			continue
		}
	verify:
		for _, reverseIndex := range indices {
			distance := totalLength - reverseIndex - i
			if distance < maxLength {
				continue
			}
			if distance == maxLength && len(result) == 0 {
				result = string(toSearch)
				continue
			}
			target := distance / 2
			for v := len(toSearch) - 1; v <= target; v++ {
				if b[v+i] != b[totalLength-v-reverseIndex-1] {
					continue verify
				}
			}
			maxLength = distance
			result = string(b[i : totalLength-reverseIndex])
		}
	}
	return result
}

func reverse(b []byte) []byte {
	length := len(b)
	reverse := make([]byte, length)
	for i := 0; i < length; i++ {
		reverse[i] = b[length-1-i]
	}

	return reverse
}
