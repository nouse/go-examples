package fib

import (
	"testing"
)

func BenchmarkInterateAdding(b *testing.B) {
	for i := 0; i < b.N; i++ {
		IterateAdding(1000000)
	}
}

func BenchmarkFastDoubling(b *testing.B) {
	for i := 0; i < b.N; i++ {
		FastDoubling(1000000)
	}
}

func TestInterateAdding(t *testing.T) {
	r := IterateAdding(10)

	if r.Int64() != 55 {
		t.Errorf("Expect InterateAdding(10) return 55, actual: %d", r.Int64())
	}
}

func TestFastDoubling(t *testing.T) {
	r := FastDoubling(10)

	if r.Int64() != 55 {
		t.Errorf("Expect InterateAdding(10) return 55, actual: %d", r.Int64())
	}
}
