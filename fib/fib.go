// Package fib provides comparison of fast doubling and simple interate adding.
// Run `go test -bench=. gitlab.com/nouse/go-examples/fib` to see comparison.
package fib

import (
	"gitlab.com/nouse/go-examples/fib/internal"

	"math/big"
)

// IterateAdding just simple calculate Fibonacci with adding, which is O(n) complexity
func IterateAdding(n int) *big.Int {
	a := big.NewInt(0)
	b := big.NewInt(1)

	for i := 0; i < n; i++ {
		a.Add(a, b)
		a, b = b, a
	}
	return a
}

// FastDoubling calculate Fibonacci faster by calculate F(n/2), which is O(log n) complexity
func FastDoubling(n int) *big.Int {
	a, _, _ := internal.FastDoubling(n)
	return a
}
