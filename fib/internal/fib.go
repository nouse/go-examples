// Internal package provides an internal FastDoubling which reused *big.Int to reduce memory allocation.
package internal

import "math/big"

// (Private) Returns the tuple (F(n), F(n+1)).
func FastDoubling(n int) (*big.Int, *big.Int, *big.Int) {
	if n == 0 {
		return big.NewInt(0), big.NewInt(1), new(big.Int)
	}

	a, b, c := FastDoubling(n / 2)

	odd := n % 2

	if odd == 0 {
		// (2*b - a)*a
		c.Lsh(b, 1)
		c.Sub(c, a)
		c.Mul(c, a)
	} else {
		// (2*a + b)*b
		c.Lsh(a, 1)
		c.Add(c, b)
		c.Mul(c, b)
	}

	// a*a + b*b
	a.Mul(a, a)
	b.Mul(b, b)
	b.Add(b, a)

	if odd == 0 {
		return c, b, a
	}

	return b, c, a
}
