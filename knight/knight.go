// Package knight solves 1 solution of knight tour problem
// under Warndorf's rule.
//
// Reference: https://en.wikipedia.org/wiki/Knight%27s_tour#Warnsdorf's_rule
package knight

import (
	"fmt"
	"math/rand"
	"sort"
	"strings"
	"time"
)

// Knight represent current state
type Knight struct {
	current Coord
	paths   []Coord
	board   *Board
}

// Make a knight initial state with a clean board and an initial knight
func MakeKnight(size int) *Knight {
	r := rand.New(rand.NewSource(time.Now().UnixNano()))
	board := Board{
		width:  size,
		height: size,
		grids:  make([][]bool, size),
	}
	for i := 0; i < size; i++ {
		board.grids[i] = make([]bool, size)
	}
	return &Knight{
		current: Coord{r.Intn(size), r.Intn(size)},
		board:   &board,
	}
}

// Solve knight tour problem based on current state
func (knight *Knight) Tour() {
	knight.paths = append(knight.paths, knight.current)
	x, y := knight.current.x(), knight.current.y()
	knight.board.grids[x][y] = true

	next := knight.board.Available(knight.current)

	if len(next) == 0 {
		return
	}
	choices := Choices{
		grids: next,
		board: knight.board,
	}
	sort.Sort(choices)
	knight.current = choices.grids[0]
	knight.Tour()
}

// PrintPath will print steps of the solution.
func (knight *Knight) PrintPath() string {
	grids := make([][]int, knight.board.height)

	for i := 0; i < len(grids); i++ {
		grids[i] = make([]int, knight.board.width)
	}

	index := 1
	for _, grid := range knight.paths {
		grids[grid.x()][grid.y()] = index
		index++
	}

	var buf strings.Builder
	for _, line := range grids {
		buf.WriteString("|")
		for _, index := range line {
			buf.WriteString(fmt.Sprintf("%03d|", index))
		}
		buf.WriteString("\n")
	}
	return buf.String()
}
