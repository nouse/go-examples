// Print a solution of knight tour problem with given width and height
package main

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"gitlab.com/nouse/go-examples/knight"
)

func main() {
	args := os.Args
	if len(args) != 2 {
		log.Fatal("Usage: knight size")
	}
	size, err := strconv.Atoi(args[1])
	if err != nil || size <= 0 {
		log.Fatalf("Invalid size: %s", args[1])
	}

	k := knight.MakeKnight(size)
	k.Tour()
	fmt.Print(k.PrintPath())
}
