package knight

// Board represents a chess board
type Board struct {
	width  int
	height int
	grids  [][]bool
}

// Coord is the location on board
type Coord [2]int

func (c Coord) x() int { return c[0] }
func (c Coord) y() int { return c[1] }

// Available returns available grids of next ste
func (board *Board) Available(g Coord) []Coord {
	x, y := g.x(), g.y()
	grids := [8]Coord{
		{x - 1, y - 2},
		{x - 1, y + 2},
		{x - 2, y - 1},
		{x - 2, y + 1},
		{x + 1, y - 2},
		{x + 1, y + 2},
		{x + 2, y - 1},
		{x + 2, y + 1},
	}

	next := []Coord{}
	for _, grid := range grids {
		if grid.x() >= 0 && grid.x() < board.width &&
			grid.y() >= 0 && grid.y() < board.height &&
			!board.grids[grid.x()][grid.y()] {
			next = append(next, grid)
		}
	}
	return next
}
