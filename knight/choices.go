package knight

// Choices is used to sort next available steps to select next grid contains fewest onward moves.
type Choices struct {
	grids []Coord
	board *Board
}

func (choices Choices) Len() int {
	return len(choices.grids)
}

func (choices Choices) Less(i, j int) bool {
	grid1 := choices.grids[i]
	grid2 := choices.grids[j]
	return len(choices.board.Available(grid1)) < len(choices.board.Available(grid2))
}

func (choices Choices) Swap(i, j int) {
	swap := choices.grids[j]
	choices.grids[j] = choices.grids[i]
	choices.grids[i] = swap
}
