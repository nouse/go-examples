package cache

type valueNIndex [2]int

type LRUCache struct {
	capacity int
	table    map[int]valueNIndex
	// Save key in a order list
	// list[0] is oldest
	list []int
	// Current tail position
	tail int
	// store staled state
	staled []bool
	// Record current size
	size int
}

func NewLRUCache(capacity int) LRUCache {
	return LRUCache{
		capacity: capacity,
		list:     make([]int, capacity*2),
		table:    make(map[int]valueNIndex, capacity),
		staled:   make([]bool, capacity*2),
	}
}

func (c *LRUCache) clean() {
	if c.tail < 2*c.capacity {
		return
	}
	list := make([]int, c.capacity*2)
	var current int
	for index, staled := range c.staled {
		if staled {
			continue
		}
		key := c.list[index]
		list[current] = key
		v := c.table[key]
		c.table[key] = valueNIndex{current, v[1]}
		current++
	}
	c.tail = current
	c.staled = make([]bool, c.capacity*2)
	c.list = list
}

func (c *LRUCache) stale(index, key int) {
	c.staled[index] = true
	c.list[c.tail] = key
	v := c.table[key]
	c.table[key] = valueNIndex{c.tail, v[1]}
	c.tail++
	c.clean()
}

func (c *LRUCache) Get(key int) int {
	v := c.table[key]
	if v[1] == 0 {
		return -1
	}
	index := v[0]
	if index == c.tail-1 {
		return v[1]
	}
	c.stale(index, key)
	return v[1]
}

func (c *LRUCache) Put(key int, value int) {
	v := c.table[key]
	if v[1] != 0 {
		// Move index to newest
		index := v[0]
		c.table[key] = valueNIndex{index, value}
		if index == c.tail-1 {
			return
		}
		c.stale(index, key)
		return
	}

	if c.size == c.capacity {
		for index, key := range c.list {
			if c.staled[index] {
				continue
			}
			c.staled[index] = true
			delete(c.table, key)
			break
		}
	} else {
		c.size++
	}
	c.list[c.tail] = key
	c.table[key] = valueNIndex{c.tail, value}
	c.tail++
	c.clean()
}
