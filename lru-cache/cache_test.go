package cache

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
	"testing"
)

func TestFromTestData(t *testing.T) {
	testfiles := []string{
		"basic",
		"basic2",
		"basic3",
		"complex",
		"complex2",
	}
	var command []string
	var key, value, expected, capacity int
	for _, testfile := range testfiles {
		t.Run(testfile, func(it *testing.T) {
			var err error
			file, err := os.Open(fmt.Sprintf("testdata/%s.txt", testfile))
			if err != nil {
				it.Fatal(err)
			}
			defer file.Close()
			scanner := bufio.NewScanner(file)
			if !scanner.Scan() {
				it.Logf("skip empty file %s", testfile)
			}
			capacity, err = strconv.Atoi(scanner.Text())
			if err != nil {
				it.Fatal(err)
			}
			cache := NewLRUCache(capacity)
			for scanner.Scan() {
				line := scanner.Text()
				command = strings.SplitN(line, " ", 3)
				switch command[0] {
				case "put":
					key, err = strconv.Atoi(command[1])
					if err != nil {
						it.Logf("skip line %s", line)
					}
					value, err = strconv.Atoi(command[2])
					if err != nil {
						it.Logf("skip line %s", line)
					}
					cache.Put(key, value)
				case "get":
					key, err = strconv.Atoi(command[1])
					if err != nil {
						it.Logf("skip line %s", line)
					}
					expected, err = strconv.Atoi(command[2])
					if err != nil {
						it.Logf("skip line %s", line)
					}
					value = cache.Get(key)
					if expected != value {
						it.Errorf("Expected get %d with %d, actual %d", key, expected, value)
					}
				default:
					it.Logf("skip line %s", line)
				}
			}
		})
	}
}
