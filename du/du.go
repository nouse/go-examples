package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path"
	"strconv"
	"strings"
)

func main() {
	var path string
	if len(os.Args) == 1 {
		path = "."
	} else {
		path = os.Args[1]
	}
	stat, err := os.Lstat(path)
	if err != nil {
		log.Fatal(err)
	}
	if !stat.IsDir() {
		fmt.Printf("%s %d", path, stat.Size())
		os.Exit(0)
	}
	entries, total, err := usage(path)
	if err != nil {
		log.Fatal(err)
	}
	writer := bufio.NewWriter(os.Stdout)
	defer writer.Flush()

	for _, e := range entries {
		writer.WriteString(formatSize(e.size))
		writer.WriteString(e.name)
		writer.WriteString("\n")
	}
	writer.WriteString(formatSize(total))
	writer.WriteString(path)
	writer.WriteString("\n")
}

type s struct {
	name string
	size int64
}

func formatSize(size int64) string {
	s := strconv.FormatInt(size, 10)
	return s + strings.Repeat(" ", 8-len(s))
}

func usage(directory string) (entries []s, total int64, err error) {
	f, err := os.Open(directory)
	if err != nil {
		return
	}
	infos, err := f.Readdir(-1)
	f.Close()
	if err != nil {
		return
	}
	for _, info := range infos {
		realpath := path.Join(directory, info.Name())
		if info.IsDir() {
			total += info.Size()
			innerEntries, innerTotal, innerError := usage(realpath)
			if innerError != nil {
				err = innerError
			}
			entries = append(entries, s{name: directory, size: innerTotal})
			entries = append(entries, innerEntries...)
			total += innerTotal
		} else {
			size := info.Size()
			entries = append(entries, s{name: realpath, size: size})
			total += size
		}
	}
	return
}
