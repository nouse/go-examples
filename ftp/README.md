# A work-in-progress FTP Server written in Go

## Implemented Commands

- PWD
- SYST
- PORT
- LIST
- RETR
- TYPE

## Usage

    go build -o ftp gitlab.com/nouse/go-examples/ftp/cmd/ftp
    ./ftp

    ftp -4v ftp://user@localhost:5555
    Connected to localhost.
    200 OHAI
    230 Logged in anonymously
    Remote system type is GoFTPd.
    200 TYPE CHANGED.
    ftp> ls
    ...
    ftp> get q
    local: q remote: q
    200 PORT Command Successfully
    125 Transfer Starting
      2106 KiB  326.18 MiB/s
    226 Closing data connection, sent 2157520 bytes.
    2157520 bytes received in 00:02 (955.08 KiB/s)
