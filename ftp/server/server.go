package server

import (
	"bufio"
	"io"
	"strings"

	"gitlab.com/nouse/go-examples/ftp/client"
)

func (s Server) HandleConn(conn io.ReadWriteCloser) {
	conn.Write([]byte("200 OHAI\r\n"))
	buf := bufio.NewScanner(conn)
	handler := client.Client{
		Dir:  s.Path,
		Conn: conn,
	}
	for buf.Scan() {
		commandLine := buf.Text()
		println(commandLine)
		commands := strings.SplitN(commandLine, " ", 2)
		switch commands[0] {
		case "USER":
			handler.User()
		case "PWD":
			handler.Pwd()
		case "SYST":
			conn.Write([]byte("215 GoFTPd\r\n"))
		case "PORT":
			handler.ConnectDataSocket(commands[1])
		case "LIST":
			handler.List()
		case "RETR":
			handler.Retr(commands[1])
		case "STOR":
			handler.Stor(commands[1])
		case "TYPE":
			conn.Write([]byte("200 TYPE CHANGED.\r\n"))
		default:
			conn.Write([]byte("502 Not implemented\r\n"))
		}
	}
}
