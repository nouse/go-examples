package main

import (
	"log"
	"net"
	"os"

	"gitlab.com/nouse/go-examples/ftp/server"
)

func main() {
	listener, err := net.Listen("tcp", ":5555")
	if err != nil {
		log.Fatal(err)
	}
	defer listener.Close()
	currentDir, err := os.Getwd()
	if err != nil {
		log.Fatal(err)
	}
	s := server.Server{currentDir}
	for {
		conn, err := listener.Accept()
		if err != nil {
			log.Print(err)
			continue
		}
		go s.HandleConn(conn)
	}
}
