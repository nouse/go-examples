package client

import (
	"bufio"
	"fmt"
	"io"
	"net"
	"os"
	"strconv"
	"strings"
)

type Client struct {
	Dir      string
	Conn     io.ReadWriter
	dataConn io.ReadWriteCloser
}

func (c *Client) ChangeDir(relativePath string) {
}

// ConnectDataSocket is FTP active mode
func (c *Client) ConnectDataSocket(hostPort string) {
	array := strings.Split(hostPort, ",")
	if len(array) != 6 {
		c.Conn.Write([]byte("501 Invalid argument of PORT\r\n"))
		return
	}
	host := strings.Join(array[0:4], ".")
	high, err := strconv.Atoi(array[4])
	if err != nil {
		c.Conn.Write([]byte("501 Invalid argument\r\n"))
		return
	}
	low, err := strconv.Atoi(array[5])
	if err != nil {
		c.Conn.Write([]byte("501 Invalid argument\r\n"))
		return
	}
	port := high*256 + low
	conn, err := net.Dial("tcp", fmt.Sprintf("%s:%d", host, port))
	if err != nil {
		c.Conn.Write([]byte("550 Connect failed\r\n"))
		return
	}
	c.dataConn = conn
	c.Conn.Write([]byte("200 PORT Command Successfully\r\n"))
}

func (c *Client) List() {
	dir, err := os.Open(c.Dir)
	if err != nil {
		c.Conn.Write([]byte("550 Can't open current dir\r\n"))
		return
	}
	defer dir.Close()
	c.Conn.Write([]byte("125 Transfer Starting\r\n"))
	entries, err := dir.Readdirnames(0)
	if err != nil {
		c.Conn.Write([]byte("550 Can't open current dir\r\n"))
		return
	}
	writer := bufio.NewWriter(c.dataConn)
	total := 0
	for _, name := range entries {
		total += len(name)
		total += 2
		writer.WriteString(name)
		writer.WriteString("\r\n")
	}
	writer.Flush()
	c.dataConn.Close()
	fmt.Fprintf(c.Conn, "226 Closing data connection, sent %d bytes.\r\n", total)
}

func (c *Client) Retr(file string) {
	f, err := os.Open(c.Dir + "/" + file)
	if err != nil {
		c.Conn.Write([]byte("550 Can't open file.\r\n"))
		return
	}
	fileinfo, err := f.Stat()
	if err != nil {
		c.Conn.Write([]byte("550 Can't get file info.\r\n"))
		return
	}
	size := fileinfo.Size()
	c.Conn.Write([]byte("125 Transfer Starting\r\n"))
	go func(w io.WriteCloser, r io.ReadCloser) {
		io.Copy(w, r)
		w.Close()
		r.Close()
	}(c.dataConn, f)
	fmt.Fprintf(c.Conn, "226 Closing data connection, sent %d bytes.\r\n", size)
}

func (c *Client) Stor(file string) {
	f, err := os.Open(c.Dir + "/" + file)
	if err != nil {
		f, err = os.Create(file)
	}
	if err != nil {
		c.Conn.Write([]byte("550 Can't read file.\r\n"))
		return
	}
	c.Conn.Write([]byte("125 Transfer Starting\r\n"))
	go func(w io.WriteCloser, r io.ReadCloser) {
		io.Copy(w, r)
		w.Close()
		r.Close()
	}(f, c.dataConn)
	fmt.Fprint(c.Conn, "226 Transfer complete.\r\n")
}

func (c *Client) User() {
	c.Conn.Write([]byte("230 Logged in anonymously\r\n"))
}

func (c *Client) Pwd() {
	fmt.Fprintf(c.Conn, "250 %s\r\n", c.Dir)
}
