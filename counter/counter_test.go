package counter_test

import (
	"fmt"
	"testing"

	"gitlab.com/nouse/go-examples/counter"
)

func TestCounter(t *testing.T) {
	c, err := counter.NewCounter("uniq")
	if err != nil {
		t.Fatalf("Unexpected error when NewCounter, %s", err)
	}
	c.Increment()
	c.Increment()

	if c.Tally() != 2 {
		t.Errorf("Expected counter is 2, actual is %d", c.Tally())
	}

	c.Increment()
	expectedString := "uniq is counted 3 times"
	if c.String() != expectedString {
		t.Errorf("Expected string is %s, actual is %s", expectedString, c.String())
	}
}

func TestEmptyCounter(t *testing.T) {
	_, err := counter.NewCounter("")
	if err == nil {
		t.Error("Should raise error, but no error is raised")
	}
}

func ExampleCounter() {
	c, _ := counter.NewCounter("example")
	c.Increment()
	c.Increment()
	fmt.Println(c.Tally())
	c.Increment()
	fmt.Println(c.String())
	// Output:
	// 2
	// example is counted 3 times
}
