// Package counter provide definition of Counter and functionality to create one Counter
//
// This package is implemention of Chapter 1.2 of Algorithms (Sedgewick R. 2016)
package counter

import (
	"errors"
	"fmt"
)

// Interface of a data type whoes values are a label and a nonnegative interger
// and whose operations are create and initialize to zero, increment by one, and examine the current value.
type Counter interface {
	Tally() int
	Increment()
	String() string
}

// NewCounter create a counter named label, will raise error if label is empty.
func NewCounter(label string) (Counter, error) {
	if label == "" {
		return nil, errors.New("label can't be empty")
	}
	return &counter{label: label}, nil
}

type counter struct {
	label   string
	counter int
}

func (c *counter) Tally() int {
	return c.counter
}

func (c *counter) Increment() {
	c.counter++
}

func (c *counter) String() string {
	return fmt.Sprintf("%s is counted %d times", c.label, c.counter)
}
